/**
* `<cells-icon-message>` displays an inline informative message, such an error, warning, info or success message with an icon.
* It has two different layouts. The default layout displays the icon above the text and the "inline" layout displays the icon
* to the left of the text.
*
* The `aria-label` of the icon is set automatically for three of the default valid types (info, warning and error), but you can override that value by setting the
* `icon-label` attribute. If no `icon-label` is set and none of the valid types is used, the icon will not have `aria-label` as it will be considered presentational.
*
* An image can be used instead of an icon by setting the `src` attribute. The image will be square.
*
* Inline level HTML tags are allowed for the message. Also custom content can be used as light DOM.
*
* __Example:__
*
* ```html
* <cells-icon-message
* type="info"
* icon="coronita:info"
* message="Informative message">
* </cells-icon-message>
* ```
*
* __Example with custom label for the icon:__
*
* ```html
* <cells-icon-message
* type="warning"
* icon-label="Important message"
* icon="coronita:alert"
* message="Lorem ipsum dolor sit amet.">
* </cells-icon-message>
* ```
*
* __Example with an image used as icon:__
*
* ```html
* <cells-icon-message
* src="http://domain/image.png"
* message="Lorem ipsum dolor sit amet.">
* </cells-icon-message>
* ```
*
* __Example with format (inline level tags):__
*
* ```html
* <cells-icon-message
* message="Lorem <i>ipsum</i> dolor sit amet.">
* </cells-icon-message>
* ```
*
* __Example using i18n Behavior:__
*
* ```html
* <cells-icon-message
* type="info"
* icon="coronita:info"
* heading="cells-icon-message-title"
* message="cells-icon-message-message">
* </cells-icon-message>
* ```
*
* Translation on `'locales/es.json'`
*
* ```json
* {
*  "cells-icon-message-title": "Hey there!",
*  "cells-icon-message-message": "I am a english message, you can use i18 behavior for translation purposes."
* }
* ```
*
* ## Icons
*
* Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html).
* In fact, this component uses an iconset in its demo.
*
* ## Styling
*
* The following custom properties and mixins are available for styling:
* ### Custom Properties
* | Custom Property                               | Selector                      | CSS Property     | Value                                                            |
* | --------------------------------------------- | ----------------------------- | ---------------- | ---------------------------------------------------------------- |
* | --cells-icon-message-icon-margin              | :host                         | --icon-margin    | 1.25rem                                                          |
* | --cells-icon-message-header-margin            | :host                         | --header-margin  | 1.25rem                                                          |
* | --cells-icon-message-header-font              | :host                         | --header-font    | 1rem                                                             |
* | --cells-fontDefault                           | :host                         | font-family      | sans-serif                                                       |
* | --cells-text-size-icon-message                | :host                         | font-size        | var(--cells-text-size-ml, rem(15px))                             |
* | --cells-text-size-icon-message                | :host(.message-input)         | font-size        | var(--cells-text-size-xs, var(--cells-text-size-12, rem(12px)))  |
* | --cells-icon-message-color                    | :host                         | color            | ![#121212](https://placehold.it/15/121212/000000?text=+) #121212 |
* | --cells-icon-message-color-theme              | :host                         | color            | ![#121212](https://placehold.it/15/121212/000000?text=+) #121212 |
* | --cells-icon-message-background-color-info    | :host([type="info"])          | background-color | ![#D4EDFC](https://placehold.it/15/D4EDFC/000000?text=+) #D4EDFC |
* | --cells-icon-message-background-color-warning | :host([type="warning"])       | background-color | ![#F3EBD5](https://placehold.it/15/fde7d8/000000?text=+) #F3EBD5 |
* | --cells-icon-message-background-color-error   | :host([type="error"])         | background-color | ![#fcdfdf](https://placehold.it/15/fcdfdf/000000?text=+) #fddfdf |
* | --cells-icon-message-background-color-success | :host([type="success"])       | background-color | ![#daefe0](https://placehold.it/15/daefe0/000000?text=+) #daefe0 |
* | --cells-icon-message-icon-color-theme         | :host                         | color            | var(--cells-icon-message-icon-color, inherit)                    |
* | --cells-icon-message-icon-color-theme         | :host([type="info"])          | background-color | ![#D4EDFC](https://placehold.it/15/D4EDFC/000000?text=+) #D4EDFC |
* | --cells-icon-message-icon-color-theme         | :host([type="warning"])       | background-color | ![#D8BE75](https://placehold.it/15/fde7d8/000000?text=+) #D8BE75 |
* | --cells-icon-message-icon-color-theme         | :host([type="error"])         | background-color | ![#fcdfdf](https://placehold.it/15/fcdfdf/000000?text=+) #fddfdf |
* | --cells-icon-message-icon-color-theme         | :host([type="success"])       | background-color | ![#daefe0](https://placehold.it/15/daefe0/000000?text=+) #daefe0 |
* | --cells-icon-message-icon-color               | .icon                         | color            | inherit                                                          |
* | --icon-margin                                 | .icon                         | margin-bottom    | `No fallback value`                                              |
* | --icon-margin                                 | :host(.inline) .icon          | margin-right     | calc( / 2)                                                       |
* | --cells-icon-message-icon-color-info          | :host([type="info"]) .icon    | color            | ![#5BBEFF](https://placehold.it/15/5BBEFF/000000?text=+) #5BBEFF |
* | --cells-icon-message-icon-color-warning       | :host([type="warning"]) .icon | color            | ![#D8BE75](https://placehold.it/15/fab27f/000000?text=+) #D8BE75 |
* | --cells-icon-message-icon-color-error         | :host([type="error"]) .icon   | color            | ![#f79698](https://placehold.it/15/f79698/000000?text=+) #f79698 |
* | --cells-icon-message-icon-color-success       | :host([type="success"]) .icon | color            | ![#48ae64](https://placehold.it/15/48ae64/000000?text=+) #48ae64 |
* | --header-margin                               | .header                       | margin-bottom    | `No fallback value`                                              |
* | --header-font                                 | .header                       | font-size        | `No fallback value`                                              |
*
* ### @apply
* | Mixins                              | Selector                       | Value |
* | ----------------------------------- | ------------------------------ | ----- |
* | --cells-icon-message                | :host                          | {}    |
* | --cells-icon-message-info           | :host([type="info"])           | {}    |
* | --cells-icon-message-warning        | :host([type="warning"])        | {}    |
* | --cells-icon-message-error          | :host([type="error"])          | {}    |
* | --cells-icon-message-success        | :host([type="success"])        | {}    |
* | --cells-icon-message-inline         | :host(.inline)                 | {}    |
* | --cells-icon-message-inline-info    | :host(.inline[type="info"])    | {}    |
* | --cells-icon-message-inline-warning | :host(.inline[type="warning"]) | {}    |
* | --cells-icon-message-inline-error   | :host(.inline[type="error"])   | {}    |
* | --cells-icon-message-inline-success | :host(.inline[type="success"]) | {}    |
* | --cells-icon-message-icon           | .icon                          | {}    |
* | --cells-icon-message-icon-inline    | :host(.inline) .icon           | {}    |
* | --cells-icon-message-icon-info      | :host([type="info"]) .icon     | {}    |
* | --cells-icon-message-icon-warning   | :host([type="warning"]) .icon  | {}    |
* | --cells-icon-message-icon-error     | :host([type="error"]) .icon    | {}    |
* | --cells-icon-message-icon-success   | :host([type="success"]) .icon  | {}    |
* | --cells-icon-message-message        | .message                       | {}    |
* | --cells-icon-message-header         | .header                        | {}    |
* | --cells-lists-h-gutter              | :host(.h-gutter)               | {}    |
*
*
* @polymer
* @customElement
* @extends {Polymer.Element}
* @demo demo/index.html
* @hero cells-icon-message.png
**/
class CellsIconMessage extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior ], Polymer.Element) {
  static get is() {
    return 'cells-icon-message';
  }
  static get properties() {
    return {
      /**
       * Message type. Used for styling purposes and to set the icon label
       * if one of the valid types if used.
       */
      type: {
        type: String,
        value: null,
        reflectToAttribute: true
      },
      /**
       * List of allowed types.
       */
      validTypes: {
        type: Array,
        value: function() {
          return [
            'info',
            'warning',
            'error',
            'success'
          ];
        }
      },
      /**
       * Size of the icon (width and height).
       * Odd values are allowed.
       */
      sizes: {
        type: Object,
        value: function() {
          return {};
        },
        observer: '_observeSizes'
      },
      /**
       * Icon ID.
       */
      icon: {
        type: String,
        value: ''
      },
      /**
       * URL of the image to be used as icon.
       */
      src: {
        type: String,
        value: ''
      },
      /**
       * heading of the message. header text.
       */
      heading: {
        type: String,
        value: null
      },
      /**
       * Text message. Inline HTML elements are allowed (`<strong>`, `<b>`, `<em>`, `<i>`, etc.)
       */
      message: {
        type: String,
        value: null
      },
      _iconLabel: {
        type: String,
        computed: '_computeIconLabel(iconLabel, type, validTypes)'
      },
      _hasIconOrImage: {
        type: Boolean,
        computed: '_computeHasIconOrImage(icon, src)'
      }
    };
  }

  static get observers() {
    return [
      '_observeSizes(sizes.*)'
    ];
  }


  _observeSizes() {
    let width = this.get('sizes.width');
    let height = this.get('sizes.height');

    if (typeof(width) !== 'string' && width) {
      width += 'px';
    }
    if (typeof(height) !== 'string' && height) {
      height += 'px';
    }
    if (!width || !height) {
      width = width || height;
      height = width;
    }

    if (width && height) {
      this.updateStyles({
        '--iron-icon-width': width,
        '--iron-icon-height': height
      });
    }
  }

  _computeIconLabel(iconLabel, type, validTypes) {
    if (iconLabel) {
      return iconLabel;
    }
    if (validTypes.indexOf(type) >= 0 && type !== 'success') {
      return `cells-icon-message-${type}`;
    }
    return false;
  }

  _computeIconLabelHidden(iconLabel) {
    return iconLabel ? false : 'true';
  }

  _computeHasIconOrImage(icon, src) {
    return icon || src;
  }
}

window.customElements.define(CellsIconMessage.is, CellsIconMessage);
