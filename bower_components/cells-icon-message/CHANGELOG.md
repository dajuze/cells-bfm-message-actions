# Changelog

## v13.0.0
- Custom icon/image sizes and remove computed inline styleS
- Added class to add horizontal margins `.h-gutter`

## v12.1.2
- Add mixin to apply CSS styles in `<strong>` tags inside message
