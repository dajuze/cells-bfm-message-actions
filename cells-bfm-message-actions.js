{
  const {
    html,
  } = Polymer;
  /**
    `<cells-bfm-message-actions>` Description.

    Example:

    ```html
    <cells-bfm-message-actions></cells-bfm-message-actions>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-bfm-message-actions | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsBfmMessageActions extends Polymer.Element {

    static get is() {
      return 'cells-bfm-message-actions';
    }

    static get properties() {
      return {
        texto_mensaje: {
          type: String,
          value: 'Añada más empresas para tener una visión más completa de su facturación.'
        },
        tx_btn_agregar: {
          type: String,
          value: 'Agregar empresa'
        },
        tx_btn_despues: {
          type: String,
          value: 'Más tarde'
        }
      };
    }
  }

  customElements.define(CellsBfmMessageActions.is, CellsBfmMessageActions);
}